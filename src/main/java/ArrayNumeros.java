import java.util.Scanner;
public class ArrayNumeros {
    public static void main (String[] args) {
        Scanner sc = new Scanner (System.in);
        
        int contador = 0;
        int suma = 0;
        int arraynumeros[] = new int[10];
        
        for (int n1 = 0; n1 < 10; n1++ ) {
            System.out.println("Introduzca un número");
            int numero = sc.nextInt();
            suma = suma+numero;
            contador = contador+1;
            arraynumeros[n1] = numero;
            System.out.println("La media hasta ahora es de: "+suma/contador);
        }
    }
}
